# なかま１

# 1｜　

### Daily greetings

- おはよう。　*Good morning.*
  - おはようございます。
- こんにちは。　*Good afternoon.*
- こんばんは。　*Good evening.*
- おやすみ。　*Good night.*
  - おやすみなさい。
- 初めまして。～です。どうぞよろしく。　*How do you do? I am ～. Pleased to meet you.*

### Taking leave of friends and instructors

- [失]{しつ}[礼]{れい}します。　*Good-bye. / Excuse me.*
- じゃあ、また。　*See you later.*
- さようなら。／さよなら。　*Good-bye.*

### Thanking, apologizing, and getting attention

- ありがとう。　*Thank you.*
  - ありがとうございます。　
- どういたしまして。　*You are welcome.*
- すみません。　*I'm sorry, excuse me.*
  - （あのう、）すみません。　*(Um,) Excuse me.*

### Confirming information and making requests

- 分かりましたか。　*Do you understand (it)?*
- はい、分かりました。　*Yes, I understand (it).*
- いいえ、分かりません。　*No, I don't understand (it).*
- 言ってください。　*Please say it. / Repeat after me.*
- もう一度言って下さい。　*Please say it again.*
  - もう一度お願いします。
- 大きい声で言って下さい。　*Please speak loudly.*
  - 大きい声でお願いします。　
- もう少しゆっくりお願いします。　*Please speak more quietly.*
- 書いて下さい。　*Please write.*
- 聞いて下さい。　*Please listen.*
- 見て下さい。　*Please look at it.*
- 読んで下さい。　*Please read.*

### Asking for Japanese words and English equivalents

- これ・それ・あれ・～は日本語で何と言いますか。　*How do you say this/that/that over there in Japanese?*
- ～は日本語で何と言いますか。　*How do you say ～ in Japanese?*
- ～って何ですか。　*What does ～ mean?*
- ～と言います。／～って言います。　*You say ～. / You call it ～.*

# 2｜[挨]{あい}[拶]{さつ}と[自]{じ}[己]{こ}[紹]{しょう}[介]{かい}　　*Greetings and Introductions*

- いいえ、そうじゃないです　
  - いいえ、そうじゃありません
- ええ／はい、そうです　
- ～から来ました　
- こちらこそ
- そうですか
- ～って言います
- どうもありがとうごさいます
- 何処から来ましたか
- 何方からいらっしゃいましたか

## I. Identifying someone or something using **～は～です**

- ～は～です
- じゃないです。
  - じゃありません。

### Affirmative:
| Topic | | Comment | |
|---|---|---|---|
| Noun| Particle| Noun| Copula Verb|
|[田]{た}[中]{なか}さん| **は**|[三]{さん}[年]{ねん}[生]{せい}|**です**。|

> *Mx. Tanaka is a junior.*

### Negative: 
| Topic | | Comment | |
|---|---|---|---|
| Noun| Particle| Noun| Copula Verb|
|[鈴]{す}[木]{ずき}さん|**は**|[留]{りゅう}[学]{がく}[生]{せい}|**じゃありません**。|
||||**じゃないです**。|

> *Mx. Suzuki is not an international student.*

## II. Asking はい／いいえ questions, using **～は～ですか**

- ～は～ですか
- いいえ、そうじゃないです　
  - いいえ、そうじゃありません
- ええ／はい、そうです　

### Asking the listener's identity:

|Q　| | |
|---|---|---|
||Copula Verb|Particle|
|[鈴]{す}[木]{ずき}さん|**です**|**か**。|
> *Are you Mx. Suzuki?*

|A| | |
|---|---|---|
|||Copula Verb|
|**はい／ええ**、|**そう**|**です**。|
> *Yes, I am.*

### Asking about people and things:

|Q||
|---|---|
||Particle|
|キムさん**は**[韓]{かん}[国]{こく}[人]{じん}**です**|**か**。|
*Is Mx. Kim Korean? / Are you Korean, Mx. Kim?*

|A (Affirmative)| | |
|---|---|---|
|||Copula Verb|
|**はい／ええ**、|**そう**|**です**。|
|**はい／ええ**、|[韓]{かん}[国]{こく}[人]{じん}|**です**。|

> *Yes, they are. / Yes, I am.*

> *Yes, they are Korean. / Yes, I am Korean.*

|A (Negative)| | |
|---|---|---|
|||Copula Verb|
|**いいえ**、|**そう**|**じゃありません**。|
|||**じゃないです**。|
|**いいえ**、|イギリス人|**です**。|

> *No, they aren't.*

> *No, they are British. / No, I am British.*

## III. Indicating relationships between nouns with **の**

- の

## IV. Asking for personal information, using question words 
- [何]{なん}・[何]{なん}[年]{ねん}[生]{せい}・[何]{なん}[時]{じ}ですか
- [何処]{どこ}からきましたか
  - [何方]{どちら}からいらっしゃいましたか
  
### Asking about names and things, using **[何]{なん}**:

|Q|||
|---|---|---|
||Q. Word||
|お[名]{な}[前]{まえ}**は**|**[何]{なん}**|**ですか**。|

> *What is your name?*

|A|
|---|
|アリス**です**。|

> *I am Alice.*

|Q|||
|---|---|---|
||Q Word + Suffix||
|キムさん**は**|**[何]{なん}[年]{ねん}[生]{せい}**|**ですか**。|

> *What year are you in, Mx. Kim?*

|A|
|---|
|[三]{さん}[年]{ねん}[生]{せい}**です**。|

> *I am a junior.*

|Q|||
|---|---|---|
||Q Word + Suffix||
|いま**は**|**[何]{なん}[時]{じ}**|**ですか**。|

> *What time is it now?*

|A|
|---|
|[三]{さん}[時]{じ}**です**。|

> *It's 3 o'clock.*

### Using **[何処]{どこ}** and **[何方]{どちら}** to ask about places:

|Q|||
|---|---|---|
|Q Word (place)|Particle||
|**[何処]{どこ}**|**から**|**きましたか**。|
|**[何方]{どちら}**|**から**|**いらっしゃいましたか**。|

> *Where are you from?*

|A|||
|---|---|---|
|Noun (place)|Particle||
|日本|**から**|**きました**。|

> *I'm from Japan.*

## V. Using **も** to list and describe similarities

- も

# 3｜[毎]{まい}[日]{にち}の[生]{せい}[活]{かつ}　*Daily Routines*

## I. Talking about routines, future actions, or events using the polite present form of verbs and the particles **に**, **へ**, **を**, or **で**

### A. Polite present form of verbs

### B. Direct object particle, **を**

|Topic|Direct object||Verb (action)|
|---|---|---|---|
||Noun|Particle||
|私は|昼ご飯|**を**|食べます。|

> *I eat lunch. / I will eat lunch.*

### C. Destination or goal particles **に** and **へ**

|Destination/Goal||Verb|
|---|---|---|
|Place noun|Particle||
|うち|**に**／**へ**|帰ります。|

> *I go home. / I will go home. / I am going home.*

### D. Place of action and event, で

|Place of action||Verb phrase (action)|
|---|---|---|
|Noun|Particle||
|図書館|**で**|映画を見ます。|

> *I see a movie at the library. / I am going to see a movie at the library.*

## II. Presenting objects or events using **〜があります**

## III. Telling time using the particle **に**

|Point in time|||
|---|---|---|
|Time|Particle||
|六時半|**に**|起きます。|

## IV. Using adverbs to express frequency of actions

- いつも　*always*
- 大抵　*usually*
- よく　*often*
- 時々　*sometimes*
- あまり　*not very often*
- 全然　*not at all*

## V. Expressing past actions and events using the polite past form of verbs

Polite past forms:

||Affirmative||Negative|||
|---|---|---|---|---|---|
||present|past|present|past||
||～ます|～**ました**|～ません|～**ませんでした**||
|行く *to go*|行きます|行き**ました**|行きません|行き**ませんでした**|五段|
|食べる *to eat*|食べます|食べ**ました**|食べません|食べ**ませんでした**|一段|
|來る *to come*|来ます|来**ました**|来ません|来**ませんでした**||
|する *to do*|します|し**ました**|しません|し**ませんでした**||
