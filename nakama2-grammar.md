# なかま 2 [文法]{ぶんぽう}

# [第]{だい}一[課]{か}：[健康]{けんこう}



## I. Potential form 

What a person can do, what is possible.

を ---> が

> かたが[痛]{いた}くて | 手 | が       | うごかせません。
> 
> My shoulder hurts and I cannot move my hand.

<!--
|                      |    | Particle | Verb potential form (neg.) |
|----------------------|----|----------|----------------------------|
| かたが[痛]{いた}くて | 手 | が       | うごかせません             |
-->



### Potential form of る-verbs

る ---> られる

- ら can be omitted in conversation

### Potential form of う-verbs

/u/ ---> /e/る

### Potential form of irregular verbs

する

[来]{く}る

- Potential forms of all verbs conjugate as る-verbs
- No potential form for 分かる

## II. ～すぎる

～すぎる expresses excessiveness.



## III. ～たらどうですか　～方がいいです

## IV. ～ないで下さい

## V. ～てはいけない　～てもいい

